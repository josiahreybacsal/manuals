************
Writing Text
************

|image0| :kbd:`F8` or :kbd:`T`

Once the Text tool is active, you will have two options at your hands
about how to create a text.

When you want to add a text that consists only of a single word or a short expression, the easiest way to add it is to:

#. Left-click on the canvas to place the cursor at the desired position.
#. Type the text directly afterwards. The text will all be put into a
   single line, unless you hit :kbd:`Enter` to continue in the next line.

When you want to put your (longer) text into a specific area
that you have reserved for it:

#. Click-and-drag on the canvas. This will create a frame for a flowed
   text with a blue border, in which the text can be inserted.
#. Type on the keyboard. The text will appear in its frame and will not
   leave the frame area. When it reaches the border of the frame, it
   will automatically flow into the next line. The border of the text
   frame will turn red when the text doesn't completely fit into it.
#. You can change the dimensions of the text box by dragging on the
   white handle in the bottom right corner.

.. Note:: The text will not stretch or grow to fill the allotted space at all
  costs. It is up to you to adapt the size of the area or the font to your
  needs.

.. figure:: images/writing_text_canvas.png
    :alt: Writing text directly to the canvas
    :class: screenshot

    A left-click on the canvas with the text tool positions the cursor.

.. figure:: images/writing_text_newline.png
    :alt: Using return to create a newline
    :class: screenshot

    This mode does not automatically break lines in the text.

.. figure:: images/writing_text_frame.png
    :alt: Creating a text frame
    :class: screenshot

    You need to create a text frame with the text tool first, if you want to use a flowed text with automatic line breaks.

.. figure:: images/writing_text_fit.png
    :alt: Modifying the frame so the text fits
    :class: screenshot

    At any time, you can modify the text frame using its handle.

.. |image0| image:: images/outil-texte.png
